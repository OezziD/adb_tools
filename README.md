- [adb_tools](#adb_tools)
  - [How to create Project](#how-to-create-project)
  - [Helpful Links](#helpful-links)

# adb_tools

## How to create Project
`git clone git@bitbucket.org:OezziD/adb_tools.git`

then go into the directory adb_tools and execute the command
`flutter create .`

afterwards you can start the application for linux, mac and win by 
`flutter run`

you can choose the platform for which to make it run

## Helpful Links
Please have a look at the Confluence Web Pages [confluence](https://tillerstack.atlassian.net/wiki/spaces/~120088074/pages/1508278275/Android+Google+Glass+Commands+and+Utilities)
