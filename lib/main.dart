import 'dart:io';

import 'package:desktop_window/desktop_window.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(key: Key("blubb"), title: 'ADB Tool'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({required Key key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController txtEditCtrl = TextEditingController(text: "com.tillerstack.hwg_glass");

  @override
  void initState() {
    super.initState();
    DesktopWindow.setWindowSize(Size(351, 701));
    DesktopWindow.setWindowSize(Size(350, 700));
    DesktopWindow.setMinWindowSize(Size(350, 700));
    DesktopWindow.setMaxWindowSize(Size(350, 700));
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Center(child: Text(widget.title)),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextField(
              textAlign: TextAlign.center,
              controller: txtEditCtrl,
            ),
            SizedBox(
              width: 20,
              height: 20,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                padding: EdgeInsets.all(20),
                textStyle: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
                primary: Colors.green,
              ),
              onPressed: () {
                var packageName = txtEditCtrl.text;
                execute("adb",
                    ["shell", "monkey", "-p", "'$packageName'", "-c", "'android.intent.category.LAUNCHER'", "1"]);
              },
              child: Text("Start App"),
            ),
            SizedBox(
              width: 20,
              height: 20,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                padding: EdgeInsets.all(20),
                textStyle: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
                primary: Colors.red,
              ),
              onPressed: () {
                var packageName = txtEditCtrl.text;
                execute("adb", ["shell", "am", "force-stop", "$packageName"]);
              },
              child: Text("Kill App"),
            ),
            SizedBox(
              width: 20,
              height: 20,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                padding: EdgeInsets.all(20),
                textStyle: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
                primary: Colors.blue,
              ),
              onPressed: () {
                execute("scrcpy", ["-s", "1QNTGG2519330454"]);
                // execute("scrcpy", []);
              },
              child: Text("Show Android Screen"),
            ),
            SizedBox(
              width: 20,
              height: 20,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                padding: EdgeInsets.all(20),
                textStyle: TextStyle(
                  color: Colors.white,
                    fontWeight: FontWeight.bold,
                ),
                primary: Colors.pink,
              ),
              onPressed: () {
                execute("adb", ["shell", "svc", "usb", "setFunction", "mtp", "true"]);
              },
              child: Text("Set MTP on USB"),
            ),
            SizedBox(
              width: 20,
              height: 20,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                padding: EdgeInsets.all(20),
                textStyle: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
                primary: Colors.black45,
              ),
              onPressed: () {
                execute("adb", ["shell", "rm", "/sdcard/Movies/*"]);
              },
              child: Text("Delete Movies Folder"),
            ),
            SizedBox(
              width: 20,
              height: 20,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                padding: EdgeInsets.all(20),
                textStyle: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
                primary: Colors.deepPurple,
              ),
              onPressed: () {
                execute("adb", ["tcpip", "5555"]);
                execute("adb", ["connect", "192.168.178.125:5555"]);
              },
              child: Text("Connect Remotely"),
            ),
            SizedBox(
              width: 20,
              height: 20,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                padding: EdgeInsets.all(20),
                textStyle: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
                primary: Colors.orange,
              ),
              onPressed: () {
                Process.run("rm", ["screen.png"]).then((value) {
                  Process.run("adb", ["shell", "screencap", "-p", "/sdcard/screen.png"]).then((value) {
                    Process.run("adb", ["pull", "/sdcard/screen.png"]).then((value) {
                      Process.run("adb", ["shell", "rm", "/sdcard/screen.png"]).then((value) {
                        execute("eog", ["screen.png"]);
                      });
                    });
                  });
                });
              },
              child: Text("Take Screenshot"),
            ),
          ],
        ),
      ),
// This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  void execute(String command, List<String> arguments) async {
    await Process.run(command, arguments);
  }
}
